from gevent.pywsgi import WSGIServer
from gevent import monkey
monkey.patch_all()

import logging
from contextvars import ContextVar

from app import create_app


# def entrypoint():
#     options = parse_args()
#     id_var: ContextVar = ContextVar("id_var", default="")
#     configure_logging(id_var=id_var, service_name="testtask", log_collector_url="")
#     logger = logging.getLogger()
#     logger.setLevel(logging.DEBUG)
#     logger.info("Starting test service")

#     return app

app = create_app('development')
