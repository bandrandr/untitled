## запуск
```
docker-compose build
docker-compose up
```

## запросы для Postman

### добавление нового юзера

```
POST <host>:5500/user
Body (raw)
{"username": "bashya",
"pwd": "bashya"}
```
Ответ должен быть 200)

### логин

```
POST <host>:5500/login
Auth -> Basic Auth:
- username: <username>
- password: <password>
```
В ответе токен


### получить пользователя

```GET <host>:5500/<username>?token=<token>```

В ответе моделька со всеми полями, кроме пароля


### добавить/обновить аватар или имя

```
PATCH <host>:5500/<username>?token=<token>
Body (raw)
{"avatar": "https://i06.fotocdn.net/s122/5f2846b78b0a94bd/public_pin_l/2782965955.jpg",
"name": "Bas"}
```
