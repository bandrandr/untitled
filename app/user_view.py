import json
import logging
from datetime import datetime

from flask import request, jsonify
from flask.views import MethodView
from werkzeug.exceptions import BadRequest, NotFound

from .models import User
from .login import check_for_token


class UserView(MethodView):
    __logger = logging.getLogger(__name__)

    def __init__(self, db):
        self.db = db

    @check_for_token
    def get(self, username):
        user = User.query.filter_by(username=username).first()
        if not user:
            return NotFound
        return jsonify(user.to_dict())



    @check_for_token
    def patch(self, username):
        # TODO: authorized_check
        data = request.json
        avatar = data.get('avatar')
        name = data.get('name')
        if not (avatar or name):
            return BadRequest('no data to update')

        user = User.query.filter_by(username=username).first()
        if avatar:
            user.avatar = avatar
        if name:
            user.name = name
        self.db.session.commit()
        return 'ok'


class UserCreateView(MethodView):
    __logger = logging.getLogger(__name__)

    def __init__(self, db):
        self.db = db

    def post(self):
        data = request.json
        username = data.get('username')
        pwd = data.get('pwd')
        if not (username and pwd):
            return BadRequest('no username or password provided')

        new_user = User(username=username, password=pwd)
        self.db.session.add(new_user)
        self.db.session.commit()
        return 'ok'