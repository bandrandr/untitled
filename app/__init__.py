import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from gevent.pywsgi import WSGIServer

from config import config


db = SQLAlchemy()


def create_app(mode='development'):
    app = Flask(__name__)
    app.config.from_object(config[mode])

    with app.app_context():
        db.init_app(app)

        from app.user_view import UserView
        app.add_url_rule("/<username>", view_func=UserView.as_view(
            "user",
            db,
        ))
        from app.user_view import UserCreateView
        app.add_url_rule("/user", view_func=UserCreateView.as_view(
            "create_user",
            db,
        ))
        from app.login import LoginView
        app.add_url_rule("/login", view_func=LoginView.as_view(
            "login",
            db
        ))

        db.create_all()
        # http_server = WSGIServer(('::', int(os.getenv('HTTP_PORT', 5500))), app)
        # http_server.serve_forever()
    return app


from app import models