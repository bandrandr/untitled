import jwt
import json
import logging

from functools import wraps
from flask import request, jsonify
from flask.views import MethodView
from datetime import datetime, timedelta
from werkzeug.exceptions import BadRequest, NotFound, Forbidden

from app.models import User

JWT_SECRET = 'secret'
JWT_ALGORITHM = 'HS256'
JWT_EXP_DELTA_MINUTES = 10


def check_for_token(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = request.args.get('token')
        if not token:
            return Forbidden('no token provided')
        try:
            data = jwt.decode(token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
        except (jwt.DecodeError, jwt.ExpiredSignatureError):
            return BadRequest('wrong token')
        return f(*args, **kwargs)
    return decorated


class LoginView(MethodView):
    __logger = logging.getLogger(__name__)

    def __init__(self, db):
        self.db = db

    def post(self):
        auth = request.authorization
        user = User.query.filter_by(username=auth.username).first()
        if not user:
            return NotFound('user with such username does not exist')
        if auth.password == user.password:
            payload = {
                'user_id': user.id,
                'exp': datetime.utcnow() + timedelta(minutes=JWT_EXP_DELTA_MINUTES)
            }
        jwt_token = jwt.encode(payload, JWT_SECRET, JWT_ALGORITHM)
        return jsonify({'token': jwt_token})