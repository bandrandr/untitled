from datetime import datetime

from app import db

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False, unique=True)
    create_time = db.Column(db.DateTime(timezone=True), default=datetime.now)
    update_time = db.Column(db.DateTime(timezone=True), default=datetime.now, onupdate=datetime.now)
    username = db.Column(db.String(50), nullable=False, unique=True)
    avatar = db.Column(db.String(200))
    name = db.Column(db.String(100))
    password = db.Column(db.String(100), nullable=False)

    def to_dict(self):
        return {
        'id': self.id,
        'create_time': self.create_time.isoformat(),
        'update_time': self.update_time.isoformat(),
        'username': self.username,
        'avatar': self.avatar,
        'name': self.name
        }