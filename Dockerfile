FROM python:3.9-slim

COPY config.py /

COPY requirements.txt /app/requirements.txt
RUN pip install --upgrade pip
RUN pip install -r /app/requirements.txt

COPY . /app

CMD gunicorn --worker-class gevent \
  --workers 4 \
  --bind 0.0.0.0:$HTTP_PORT \ 
  app.entrypoint:app